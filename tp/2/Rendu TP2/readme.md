# TP2 : Serveur Web

Ce deuxième TP a pour but d'approfondir la gestion de services. 

Au menu :
* serveur web qui nécessite une base de données
* une base de données
* un reverse proxy qui permet d'augmenter le niveau de sécurité global
* limiter les attaques à base de flood/spam sur tous les serveurs ayant le service SSH
* Pouvoir monitorer les ressources de chaque serveurs
* Mise en place TLS sur le Reverse Proxy


TP fait par ***Remi LARTIGAUT*** et ***Timothée DUBRAUD***

# Sommaire

<!-- vim-markdown-toc GitLab -->

* [0. Prérequis](#0-prérequis)
* [I. Base de données](#i-base-de-données)
* [II. Serveur Web](#ii-serveur-web)
* [III. Reverse Proxy](#iii-reverse-proxy)
* [IV. Un peu de sécu](#iv-un-peu-de-sécu)
    * [1. fail2ban](#1-fail2ban)
    * [2. HTTPS](#2-https)
        * [A. Génération du certificat et de la clé privée associée](#a-génération-du-certificat-et-de-la-clé-privée-associée)
        * [B. Configurer le reverse proxy](#b-configurer-le-reverse-proxy)
    * [3. Monitoring](#3-monitoring)
        * [A. Installation de Netdata](#a-installation-de-netdata)
        * [B. Alerting](#b-alerting)
        * [C. Bonus](#c-bonus)
 * [V. Script](#v-script-installation)

<!-- vim-markdown-toc -->

# 0. Prérequis

|Rôle|Adresse IP |Service|Hostname|
|-|-|-|-|
|Serveur Base de Données|`10.55.55.11`|MariaDB|`db.tp2.cesi`|
|Serveur Web |`10.55.55.12`|WordPress|`web.tp2.cesi`|
|Serveur Reverse Proxy|`10.55.55.13`|NGINX|`rp.tp2.cesi`|
|Client Vérification|`10.55.55.1`|Web Browser|`desktop.cesi`|

**fichier host client**
``` POWERSHELL
10.55.55.13    web.cesi
10.55.55.13    dbmon.cesi
10.55.55.13    webmon.cesi
10.55.55.13    rpmon.cesi
```

# I. Base de données

**Installation Paquets néccéssaire:**
``` BASH
$ sudo setenforce 0
$ sudo yum install mariadb-server
$ sudo systemctl start mariadb
$ sudo systemctl enable mariadb
```

**Initialisation MySQL:**
``` BASH
$ sudo mysql_secure_installation
OK, successfully used password, moving on...
Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.
Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!
Remove anonymous users? [Y/n] y
 ... Success!
Disallow root login remotely? [Y/n] n
 ... skipping.
Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!
Reload privilege tables now? [Y/n] y
 ... Success!
Cleaning up...
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.
``` 
**Creation Variables:**
``` BASH
$ read -p 'Enter password for new user: ' -s dbpm
$ MYSQL=`which mysql`
$ Q1="CREATE DATABASE IF NOT EXISTS WORDPRESS;"
$ Q2="CREATE USER 'lenny'@'10.55.55.12' IDENTIFIED BY '$dbpm';"
$ Q3="GRANT ALL PRIVILEGES ON WORDPRESS.* TO 'lenny'@'10.55.55.12';"
$ Q4="FLUSH PRIVILEGES;"
$ SQL="${Q1}${Q2}${Q3}${Q4}"
```
**Initialisation MariaDB plus création Base et User:**
``` BASH
$ $MYSQL -u root -p -e "$SQL"
```
**Vérification dans MariaDB:**

> **_NOTE:_** Creation Database
``` SQL
MariaDB [(none)]> USE WORDPRESS
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [WORDPRESS]>
```
> **_NOTE:_** Creation Utilisateur
``` SQL
MariaDB [WORDPRESS]> SELECT user,host FROM mysql.user;
+-------+-------------+
| user  | host        |
+-------+-------------+
| lenny | 10.55.55.12 |
| root  | 127.0.0.1   |
| root  | ::1         |
| root  | db.tp2.cesi |
| root  | localhost   |
+-------+-------------+
```
> **_NOTE:_** Attribution Persissions Utilisateur
``` MYSQL
MariaDB [(none)]> show grants for lenny@10.55.55.12;
+----------------------------------------------------------------------------------------------------------------+
| Grants for lenny@10.55.55.12                                                                                   |
+----------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON . TO 'lenny'@'10.55.55.12' IDENTIFIED BY PASSWORD '192BBC2F7C49F4504B677235167B879E0633B43C' |
| GRANT ALL PRIVILEGES ON WORDPRESS. TO 'lenny'@'10.55.55.12'                                                 |
+----------------------------------------------------------------------------------------------------------------+
```

**Ouverture Port 3306 (mysql):**
``` BASH
$ firewall-cmd --add-port=3306/tcp --permanent
$ firewall-cmd --reload
```

# II. Serveur Web

**Installation Serveur Web (httpd):**
``` BASH
$ yum -y install httpd
$ systemctl start httpd.service
$ systemctl enable httpd.service
```
``` BASH
$ systemctl status httpd.service
[root@web ~]# systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2020-12-17 15:17:38 CET; 1h 59min ago
```
**Installation php:**
``` BASH
$ yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ yum install -y yum-utils
$ yum remove -y php
$ yum-config-manager --enable remi-php56
$ yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
``` 
**Installation Wordpress:**
``` BASH
$ curl https://wordpress.org/latest.tar.gz --output wordpress_latest.tar.gz
$ yum install rsync
$ tar -zxvf wordpress_latest.tar.gz
$ rsync -avP ~/wordpress/ /var/www/hmtl
$ mkdir /var/www/html/wp-content/uploads
```

**Configuration Wordpress:**
``` BASH
$ cd /var/www/html/
$ cp wp-config-sample.php wp-config.php
$ read -p 'Enter password for user database: ' -s dbp
$ sed -i "s/'database_name_here'/'WORDPRESS'/g" wp-config.php
$ sed -i "s/'username_here'/'lenny'/g" wp-config.php
$ sed -i "s/'localhost'/'10.55.55.11'/g" wp-config.php
$ sed -i "s/'password_here'/'$dbp'/g" wp-config.php
$ sed -i "s/index.html/index.php/g" /etc/httpd/conf/httpd.conf
$ chown -R apache:apache /var/www/html/*
$ systemctl restart httpd.service
$ systemctl enable httpd.service
``` 

**Vérification Configuration wp-config.php:**
``` BASH
$ vim /var/www/html/wp-config.php
//  MySQL settings - You can get this info from your web host  //
/ The name of the database for WordPress */
define( 'DB_NAME', 'WORDPRESS' );

/ MySQL database username */
define( 'DB_USER', 'lenny') ;

/ MySQL database password */
define( 'DB_PASSWORD', 'lenny' );

/ MySQL hostname */
define( 'DB_HOST', '10.55.55.11' );

/ Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/ The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
``` 

**Ouverture port:**
``` BASH
$ firewall-cmd --add-port=80/tcp --permanent
$ firewall-cmd --reload
```
**Vérification de l'accès à Wordpress sur le navigateur:**

![](https://i.imgur.com/ZMcGzSa.png)



# III. Reverse Proxy

**Installation de epel-release et nginx:**
``` BASH
$ yum install -y epel-release
$ yum –y install nginx
$ systemctl start nginx
$ systemctl enable nginx
```
**Configuration du fichier /etc/nginx/nginx.conf:**
``` BASH
$ mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.old
$ touch /etc/nginx/nginx.conf
$ tee -a /etc/nginx/nginx.conf > /dev/null <<EOT
events {}

http {
    server {
        listen       80;
        server_name  web.cesi;
        location / {
            proxy_pass   http://10.55.55.12;
        }
    }
}
EOT
```
**Ouverture port http:**
``` BASH
$ firewall-cmd --add-port=80/tcp --permanent
$ firewall-cmd --reload
```
**Redémarrage service NGINX:**
``` BASH
$ systemctl restart nginx
$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2020-12-17 16:37:51 CET; 1s ago
```
**Vérification de la redirection sur le navigateur:**

![](https://i.imgur.com/BUxx08x.png)


# IV. Un peu de sécu


## 1. fail2ban

**Installation de fail2ban:**
``` BASH
$ yum install fail2ban-server fail2ban-firewalld
$ systemctl start fail2ban
$ systemctl enable fail2ban
$ systemctl status fail2ban
```
**Configuration du fail2ban pour ssh:**
``` BASH
$ touch /etc/fail2ban/jail.d/sshd.local
$ vim /etc/fail2ban/jail.d/sshd.local

# /etc/fail2ban/jail.d/sshd.local

[DEFAULT]
# bantime en seconde
bantime = 30
# ignore le bannissment pour l'adresse ip renseignée
ignoreip = 10.55.55.11

[sshd]
enabled = true
```
``` BASH
$ systemctl start fail2ban
$ systemctl enable fail2ban

$ fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd
```
**Test du fail2ban:**

``` BASH
$ tail -f /var/log/fail2ban.log
2020-12-17 11:18:29,345 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 11:18:29
2020-12-17 11:18:46,785 fail2ban.actions        [2012]: NOTICE  [sshd] Unban 10.55.55.1
2020-12-17 17:59:17,197 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 17:59:17
2020-12-17 17:59:20,407 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 17:59:20
2020-12-17 17:59:23,687 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 17:59:23
2020-12-17 17:59:28,647 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 17:59:28
2020-12-17 17:59:31,092 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 17:59:31
2020-12-17 17:59:31,471 fail2ban.actions        [2012]: NOTICE  [sshd] Ban 10.55.55.1
2020-12-17 17:59:33,556 fail2ban.filter         [2012]: INFO    [sshd] Found 10.55.55.1 - 2020-12-17 17:59:33
2020-12-17 17:59:51,811 fail2ban.actions        [2012]: NOTICE  [sshd] Unban 10.55.55.1
```
## 2. HTTPS

### A. Génération du certificat et de la clé privée associée

**Génération du certificat et de la clé privée:**
``` BASH
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout web.cesi.key -out web.cesi.crt
Generating a 2048 bit RSA private key
......................................................+++
..........................................................................................+++
writing new private key to 'cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:web.cesi
Email Address []:
```

**Déplacement du certificat:**
```BASH
$ mv web.cesi.crt /etc/pki/tls/certs/web.cesi.crt
$ mv web.cesi.key /etc/pki/tls/private/web.cesi.key
 ```

### B. Configurer le reverse proxy

**Configuration du fichier /etc/nginx/nginx.conf:**
```BASH
$ vim /etc/nginx/nginx.conf
events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        return 301 https://$host$request_uri;
           }

    server {
        listen       443 ssl;

        server_name web.cesi;

        ssl_certificate     /etc/pki/tls/certs/web.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/web.cesi.key;

        location / {
            proxy_pass   http://10.55.55.12/;
                   }

           }

}
```
**Ouverture port https:**
``` BASH
$ firewall-cmd --add-port=443/tcp --permanent
$ firewall-cmd --reload
```
**Redémarrage service NGINX:**
``` BASH
$ systemctl restart nginx
$ systemctl status nginx
$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2020-12-17 18:17:46 CET; 1s ago
```

**Test de la redirection en https:**

![](https://i.imgur.com/fFOjvOD.png)


## 3. Monitoring


### A. Installation de Netdata

``` BASH
$ yum install -y zlib-devel libuuid-devel libmnl-devel gcc make git autoconf $ autogen automake pkgconfig
$ yum install -y curl jq nodejs
$ yum install -y autoconf-archive cmake elfutils-libelf-devel json-c-devel  $ libtool libuv-devel lz4-devel nmap-ncat openssl-devel python3      
$ firewall-cmd --permanent --add-port=19999/tcp
$ firewall-cmd --reload
$ curl https://my-netdata.io/kickstart.sh --output netdata
$ chmod +x netdata && bash netdata

--- We are done! ---

  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata                          .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed and running now!  -'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

  enjoy real-time performance and health monitoring...

 OK
```
![](https://i.imgur.com/LrjvDR1.png)


### B. Alerting

**Création d'un serveur discord avec l'ajout d'un webhooks:**

![](https://i.imgur.com/IbywKvX.png)

**Modification du fichier /etc/netdata/health_alarm_notify.conf:**
> **_NOTE:_** On rajoute l'url du webhooks discord ainsi que le salon où vont s'afficher les messages d'alerte
``` BASH
$ /etc/netdata/edit-config health_alarm_notify.conf

# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/789127665801363457/9cxRu9OPPEu7qr_EdNdkoUekFWlYIS8U4wl7F37hyFvh
qNbz68eedBixIuNpJ9tGCjZt"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="général"
$ systemctl restart netdata
```
**Modification du fichier ram.conf:**
``` BASH
$ vi /usr/lib/netdata/conf.d/health.d/ram.conf

alarm: ram_usage
 on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
 warn: $this > 75
 crit: $this > 90
 info: The percentage of RAM used by the system.

$ systemctl restart netdata
$ systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2020-12-17 18:45:59 CET; 1min 9s ago
```
> **_NOTE:_** Pour faire le test, on a baissé le taux d'utilisation de la ram à 25%

![](https://i.imgur.com/r2IIIgZ.png)


### C. Bonus

**Génération du certificat et de la clé privée:**
> **_NOTE:_** A noter que nous avons mis *.cesi à CommonName si nous avons de futures redirections à faire
``` BASH
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout mon.cesi.key -out mon.cesi.crt
Generating a 2048 bit RSA private key
......................................................+++
..........................................................................................+++
writing new private key to 'cesi.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:
State or Province Name (full name) []:
Locality Name (eg, city) [Default City]:
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server's hostname) []:*.cesi
Email Address []:
```

**Déplacement du certificat:**
```BASH
$ mv mon.cesi.crt /etc/pki/tls/certs/mon.cesi.crt
$ mv mon.cesi.key /etc/pki/tls/private/mon.cesi.key
 ```

**Configuration du fichier /etc/nginx/nginx.conf:**
```BASH
$ vim /etc/nginx/nginx.conf
events {}

http {
    server {
        listen       80;

        server_name web.cesi;

        return 301 https://$host$request_uri;
           }

    server {
        listen       443 ssl;

        server_name web.cesi;

        ssl_certificate     /etc/pki/tls/certs/cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/cesi.key;

        location / {
            proxy_pass   http://10.55.55.12/;
                   }

           }

    server {
        listen       443 ssl;

        server_name dbmon.cesi;

        ssl_certificate     /etc/pki/tls/certs/mon.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/mon.cesi.key;

        location / {
            proxy_pass   http://10.55.55.11:19999;
                   }

           }

    server {
        listen       443 ssl;

        server_name webmon.cesi;

        ssl_certificate     /etc/pki/tls/certs/mon.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/mon.cesi.key;

        location / {
            proxy_pass   http://10.55.55.12:19999;
                   }

           }

    server {
        listen       443 ssl;

        server_name rpmon.cesi;

        ssl_certificate     /etc/pki/tls/certs/mon.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/mon.cesi.key;

        location / {
            proxy_pass   http://10.55.55.13:19999;
                   }

           }
}
```

**Redémarrage service NGINX:**
``` BASH
$ systemctl restart nginx
$ systemctl status nginx
$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since jeu. 2020-12-17 18:42:26 CET; 1s ago
```
**Test de la redirection en https:**

![](https://i.imgur.com/P5JvVJP.png)

# V. Script installation

``` BASH
$ curl https://gitlab.com/Illk/cesi-linux-2020/-/raw/master/tp/2/Rendu%20TP2/script --output && sudo chmod +x script
$ sudo bash script

1) Web           3) ReverseProxy  5) Fail2ban      7) Quit
2) MariaDB       4) NetData       6) See Menu
 Please enter your choice (6 to menu):
```
