# TP1 : Configuration système

Dans ce TP, on va passer en revue des éléments de configurations élémentaires du système.

Vous pouvez effectuer ces actions dans la première VM. On la clonera ensuite avec toutes les configurations pré-effectuées.

Au menu :
* gestion d'utilisateurs
  * sudo
  * SSH et clés
* configuration réseau
* gestion de partitions
* gestion de services

TP fait par ***Remi LARTIGAUT*** et ***Timothée DUBRAUD***

# Sommaire

<!-- vim-markdown-toc GitLab -->

* [Prérequis](#prérequis)
* [I. Utilisateurs](#i-utilisateurs)
    * [1. Création et configuration](#1-création-et-configuration)
    * [2. SSH](#2-ssh)
* [II. Configuration réseau](#ii-configuration-réseau)
    * [1. Nom de domaine et serveur DNS](#1-nom-de-domaine-et-serveur-dns)
* [III. Partitionnement](#iii-partitionnement)
    * [1. Préparation de la VM](#1-préparation-de-la-machine-virtuelle)
    * [2. Partitionnement](#2-partitionnement)
* [IV. Gestion de services](#iv-service)
    * [1. Service FireWall](#1-service-firewall)
    * [2. Service Web](#2-service-web)
    * [3. Web User](#3-web-user)

# Prérequis

**Installation de vim :**
``` BASH
sudo yum install vim-enhanced -y
```

**Désactivation de SELinux :**

``` BASH
sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
```
# I. Utilisateurs

## 1. Création et configuration
``` BASH
#Création utilisateur
useradd lenny -p mdp -d /home/lenny -s /bin/bash
#Création groupe
groupadd pigeon
#Ajout groupe dans /etc/sudoers
visudo /etc/sudoers
```
![](https://i.imgur.com/TMirJaV.png)
``` BASH
#Ajout utilisateur dans le groupe
usermod -aG pigeon lenny
```
## 2. SSH:

**Sur la machine Physique :**
``` Powershell
ssh-keygen -t rsa -b 4096
```

**Dans la VM :** 
``` BASH
cd/home/lenny
mkdir .ssh
chown -R lenny:lenny .ssh/
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+PNkrRPar1BCPK4+kbrCRqs/3dwfO1wfV5oVkZr3OPyb3cjtV2piYtgjGmFKa77Lpm1hRTwrinaQ4epbqa/Z5NmlzL/d7q7Sq3J4ow03+tA7TiUyoQuGQD05rKUaeHUeIue7lhqhwpWO9YLRWqbsAvxg7nG3vi6CYI49IjGgWpuFG60fZv/CMFTNv6BLoOAYrhARoAoXq6Hx9iA4b71yNv4AFTVxy+rNgqwJaI97cDGHERwQTJSYKptod26aa/PhaCqOCapBr2PwBKbGyVzH5y2TRI3t7ylf4HLGon5budek/W7IQs3H/oZp7SxrMHAur4XzbzgaH6F1EDuLSo6cz3UCu8WMvIfoUUw7OyhmUvd6OIO/yQRytKNpI5UsUaxh0TO7FNI0gmNKntNzrc7gdWd+21vlLMkKKSM52sUS8uVNjbBZqj/MKap0acZTIL5qYZd8FX7DI/5PfpdZFn4zY1gs4xK4jGrCOyk05wkZnDLkUMuWpJOjHDBF8kt5QHPNQLMVIztE+ijHwp9aVlefXOEIOoZQaP2Z+tC6YxDbzlR+/pA0WjhgUvPBQeA7PTiEfXuowHme5WALITItzvwE/+k38Z33zthjfGhkPylPGuU6smWxqJe/eXpcR9eyjL/KdJmvzCFMGKltMwglliKlQvCFidPPmAPlY4TbAu2KEfQ== rémi lartigaut@DESKTOP-3R9K30U'> .ssh/authorized_keys
chmod 700 .ssh -R
chmod 600 .ssh/authorized_keys
```
![](https://i.imgur.com/KCkt7NE.png)

# II. Configuration réseau

## 1. Nom de domaine et serveur DNS
``` BASH
$ echo -e 'domain lan.local\nnameserver 1.1.1.1' | tee /etc/resolv.conf
```

# III. Partitionnement

## 1. Préparation de la machine virtuelle

![](https://i.imgur.com/ONMmy4M.png)
``` BASH
$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0   40G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0   39G  0 part
  ├─centos-root 253:0    0   37G  0 lvm  /
  └─centos-swap 253:1    0    2G  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    3G  0 disk
sr0              11:0    1  973M  0 rom
```

## 2. Partitionnement

**Ajout des disques en tant que PV (Physical Volume):**
``` BASH
$ sudo pvcreate /dev/sdb
$ sudo pvcreate /dev/sdc
```
![](https://i.imgur.com/TXuBadi.png)


**Création du volume group avec les deux disques :**
``` BASH
$ vgcreate data /dev/sdb
$ vgextend data /dev/sdc
``` 
![](https://i.imgur.com/cSRFZzI.png)


**Créations des 3 LV (Logical Volum):**
``` BASH
$ lvcreate -L 1G data -n logicaldata
$ lvcreate -L 1G data -n logicaldata2
$ lvcreate -L 1G data -n logicaldata3
```
![](https://i.imgur.com/vfuiVs7.png)


**Formater les partitions en ext4 :**
``` BASH
$ mkfs -t ext4 /dev/data/logicaldata
$ mkfs -t ext4 /dev/data/logicaldata2
$ mkfs -t ext4 /dev/data/logicaldata3
``` 

**Montage des partitions:**
``` BASH
$ mkdir /mnt/part1 && mount /dev/data/logicaldata /mnt/part1
$ mkdir /mnt/part2 && mount /dev/data/logicaldata2 /mnt/part2
$ mkdir /mnt/part3 && mount /dev/data/logicaldata3 /mnt/part3
``` 

![](https://i.imgur.com/dPNITcj.png)

**Montage des partitions au démarrage:**
```bash
echo '/dev/data/logicaldata /mnt/part1 ext4 defaults 0 0' >> /etc/fstab
echo '/dev/data/logicaldata2 /mnt/part2 ext4 defaults 0 0' >> /etc/fstab
echo '/dev/data/logicaldata3 /mnt/part3 ext4 defaults 0 0' >> /etc/fstab
```


``` BASH
$ lsblk
NAME                MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda                   8:0    0   40G  0 disk
├─sda1                8:1    0    1G  0 part /boot
└─sda2                8:2    0   39G  0 part
  ├─centos-root     253:0    0   37G  0 lvm  /
  └─centos-swap     253:1    0    2G  0 lvm  [SWAP]
sdb                   8:16   0    3G  0 disk
├─data-logicaldata  253:2    0    1G  0 lvm  /mnt/part1
└─data-logicaldata2 253:3    0    1G  0 lvm  /mnt/part2
sdc                   8:32   0    3G  0 disk
└─data-logicaldata3 253:4    0    1G  0 lvm  /mnt/part3
sr0                  11:0    1  4,3G  0 rom
```

**Ouverture du port :**
``` BASH
sudo firewall-cmd --add-port=8888/tcp --permanent
```

# IV. Service

## 1. Service FireWall

**Démarrage du service firewalld :**
```bash
$ systemctl start firewalld
$ systemctl status firewalld
```
![](https://i.imgur.com/7RWxnUh.png)

**Démarrage du service firewalld au Boot du serveur :**
```BASH
$ systemctl enable firewalld
```

## 2. Service Web


``` BASH
touch /etc/systemd/system/web.service
echo -e '[Unit]\nDescription=Very simple web service\n\n[Service]\nExecStart=/bin/python2 -m SimpleHTTPServer 8888\n\n[Install]\nWantedBy=multi-user.target' | tee /etc/systemd/system/web.service
``` 
![](https://i.imgur.com/GQ2It5m.png)


**Reboot service systemctl**
``` BASH
systemctl daemon-reload
``` 
**Démarrage du service web au boot du serveur:**
``` BASH
systemctl start web
systemctl enable web
systemctl status web
``` 
![](https://i.imgur.com/wMiKebU.png)


**Ouverture du port néccéssaire au fonctionnement du service web:**
``` BASH
$ firewall-cmd --add-port=8888/tcp --permanent
$ firewall-cmd --reload
```

## 3. Web User

``` BASH
$ useradd web
$ echo 'User=web' >> /etc/systemd/system/web.service
$ echo 'WorkingDirectory=/srv' >> /etc/systemd/system/web.service
```
![](https://i.imgur.com/piBFRm6.png)

``` BASH
$ touch /srv/test
$ chown web -R /srv
$ systemctl daemon-reload
$ systemctl restart web
$ chown -R lenny:lenny .ssh/
```
![](https://i.imgur.com/2Pe6ArW.png)


``` BASH
$ curl localhost:8888/srv/
``` 
![](https://i.imgur.com/oOmNbyN.png)

