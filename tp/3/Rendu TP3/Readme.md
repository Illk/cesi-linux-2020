# TP3 : DNS, Backup et Docker

Ce TP a pour but d'étoffer l'infra montée dans le TP2 en y ajoutant quelques services d'infrastructure :
* DNS
* Sauvegarde
* appréhension de Docker avec un serveur Web conteneurisé

> Réutilisez les machines du TP2 comme support pour ce TP3.

<!-- vim-markdown-toc GitLab -->

* [I. DNS](#i-dns)
    * [1. Présentation](#1-Installation-des-paquets)
    * [2. Mise en place](#2-mise-en-place)
    * [3. Test](#3-test)
    * [4. Configuration du DNS de façon permanente](#4-configuration-du-dns-de-façon-permanente)
* [II. Backup](#ii-backup)
    * [1. Présentation](#1-présentation-1)
    * [2. Mise en place](#3-mise-en-place)
* [III. Docker](#iii-docker)

<!-- vim-markdown-toc -->

# I. DNS

## 1. Installation des paquets
``` BASH
$ yum install -y epel-release
$ yum install -y bind 
$ yum install -y bind-utils
```
## 2. Mise en place

**Prérequis**

| Machine        | IP            | Rôle                        |
|----------------|---------------|-----------------------------|
| `web.tp3.cesi` | `10.55.55.11` | Serveur Web                 |
| `db.tp3.cesi`  | `10.55.55.12` | Serveur de bases de données |
| `rp.tp3.cesi`  | `10.55.55.13` | Reverse proxy               |
| `ns1.tp3.cesi`  | `10.55.55.14` | Serveur DNS                 |



**Configuration de trois fichiers**
* Configuration du fichier /etc/named.conf
``` BASH
$ vi/etc/named.conf

options {
        listen-on port 53 { 127.0.0.1; 10.55.55.14; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { 10.55.55.0/24; };

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        /
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        / Path to ISC DLV key */
        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
[21:28]
};

zone "tp3.cesi" IN {

         type master;

         file "/var/named/tp3.cesi.db";

         allow-update { none; };
};

zone "55.55.10.in-addr.arpa" IN {

          type master;

          file "/var/named/55.55.10.db";

          allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```
* Configuration du fichier /etc/named/tp3.cesi.db
``` BASH
$ vi /etc/named/tp3.cesi.db

$TTL    604800
@   IN  SOA     ns1.tp3.cesi. root.tp3.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@      IN  NS      ns1.tp3.cesi.

ns1 IN  A       10.55.55.14
web     IN  A       10.55.55.11
db     IN  A       10.55.55.12
rp     IN  A       10.55.55.13
```

* Configuration du fichier /var/named/55.55.10.db
``` BASH
$ vi /var/named/55.55.10.db

$TTL    604800
@   IN  SOA     ns1.tp3.cesi. root.tp3.cesi. (
                                                1001    ;Serial
                                                3H      ;Refresh
                                                15M     ;Retry
                                                1W      ;Expire
                                                1D      ;Minimum TTL
                                                )

;Name Server Information
@ IN  NS      ns1.tp3.cesi.

14.55.55.10 IN PTR ns1.tp3.cesi.

;PTR Record IP address to HostName
11      IN  PTR     web.tp3.cesi.
12      IN  PTR     db.tp3.cesi.
13      IN  PTR     rp.tp3.cesi.
```
**Ouverture du trafic sur les ports 53/tcp et 53/udp**
``` BASH
$ firewall-cmd --add-port=53/tcp --permanent
$ firewall-cmd --add-port=53/udp --permanent
$ firewall-cmd --reload
```

**Démarrage du service bind (named)**
``` BASH
$ systemctl start named
$ systemctl enable named
$ systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since ven. 2020-12-18 10:41:08 CET; 10s ago
```

## 3. Test

``` BASH
$ dig web.tp3.cesi @10.55.55.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> web.tp3.cesi @10.55.55.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56144
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;web.tp3.cesi.                  IN      A

;; ANSWER SECTION:
web.tp3.cesi.           604800  IN      A       10.55.55.11

;; AUTHORITY SECTION:
tp3.cesi.               604800  IN      NS      ns1.tp3.cesi.

;; ADDITIONAL SECTION:
ns1.tp3.cesi.           604800  IN      A       10.55.55.14

;; Query time: 0 msec
;; SERVER: 10.55.55.14#53(10.55.55.14)
;; WHEN: ven. déc. 18 10:45:40 CET 2020
;; MSG SIZE  rcvd: 91

$ dig -x 10.55.55.13 @10.55.55.14

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el7_9.3 <<>> -x 10.55.55.13 @10.55.55.14
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 65146
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;13.55.55.10.in-addr.arpa.      IN      PTR

;; ANSWER SECTION:
13.55.55.10.in-addr.arpa. 604800 IN     PTR     rp.tp3.cesi.

;; AUTHORITY SECTION:
55.55.10.in-addr.arpa.  604800  IN      NS      ns1.tp3.cesi.

;; ADDITIONAL SECTION:
ns1.tp3.cesi.           604800  IN      A       10.55.55.14

;; Query time: 0 msec
;; SERVER: 10.55.55.14#53(10.55.55.14)
;; WHEN: ven. déc. 18 10:46:24 CET 2020
;; MSG SIZE  rcvd: 112
```

## 4. Configuration du DNS de façon permanente
``` BASH
$ vi /etc/resolv.conf
# Generated by NetworkManager

domain titi.wan
nameserver 1.1.1.1
nameserver 10.55.55.14
```
# II. Backup


## 1. Présentation

Le but de cette partie va être de mettre en place une sauvegarde du serveur Web en local puis une sauvegarde sur un serveur distant. 

```mermaid
graph TD;
     /var/www/html/ -- tar --> /opt/backup;
    /opt/backup -- rsync SSH --> 10.55.55.16/opt/backup/;
```

**Pour la sauvegarde, nous avons utilisé deux outils l'un natif sur linux et un autre a installer:**

* `tar`
  * utilitaire en ligne de commande
  * très puissant pour la manipulation d'archives
  * concatène au sein d'une seule et même archive
  * natif dans toutes les distributions Linux
* `rsync`
  * utilitaire en ligne de commande
  * synchronise deux dossiers
  * outil très éprouvé
  * beaucoup de documentations/articles, c'est un cas d'école

## 2. Mise en place



**Prérequis:**

 Création d'un compte utilisateur + génération clé SSH uniquement pour le backup:
 ``` BASH
$ useradd backup -p backup -d /home/lenny -s /bin/bash
# l'ajout de l'utilisateur permet d'avoir accès en écriture sur le dossier /opt/backup du serveur distant
$ usermod -aG admin lenny
```
Sur le serveur web:
``` BASH
$ ssh-keygen -t rsa -b 4096
```
Sur le serveur de backup:
``` BASH
$ cd/home/lenny
$ mkdir .ssh
$ chown -R lenny:lenny .ssh/
$ echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+PNkrRPar1BCPK4+kbrCRqs/3dwfO1wfV5oVkZr3OPyb3cjtV2piYtgjGmFKa77Lpm1hRTwrinaQ4epbqa/Z5NmlzL/d7q7Sq3J4ow03+tA7TiUyoQuGQD05rKUaeHUeIue7lhqhwpWO9YLRWqbsAvxg7nG3vi6CYI49IjGgWpuFG60fZv/CMFTNv6BLoOAYrhARoAoXq6Hx9iA4b71yNv4AFTVxy+rNgqwJaI97cDGHERwQTJSYKptod26aa/PhaCqOCapBr2PwBKbGyVzH5y2TRI3t7ylf4HLGon5budek/W7IQs3H/oZp7SxrMHAur4XzbzgaH6F1EDuLSo6cz3UCu8WMvIfoUUw7OyhmUvd6OIO/yQRytKNpI5UsUaxh0TO7FNI0gmNKntNzrc7gdWd+21vlLMkKKSM52sUS8uVNjbBZqj/MKap0acZTIL5qYZd8FX7DI/5PfpdZFn4zY1gs4xK4jGrCOyk05wkZnDLkUMuWpJOjHDBF8kt5QHPNQLMVIztE+ijHwp9aVlefXOEIOoZQaP2Z+tC6YxDbzlR+/pA0WjhgUvPBQeA7PTiEfXuowHme5WALITItzvwE/+k38Z33zthjfGhkPylPGuU6smWxqJe/eXpcR9eyjL/KdJmvzCFMGKltMwglliKlQvCFidPPmAPlY4TbAu2KEfQ== lenny@web.cesi'> .ssh/authorized_keys
$ chmod 700 .ssh -R
$ chmod 600 .ssh/authorized_keys
```

**Création du script:**
La structure du script `/opt/backup.sh` ressemble à :
```bash
#!/bin/bash
#Var
dir_to_backup="/var/www/html"
backup_path="/opt/backup/"
backu_pname="Wordpress.$(date +%F_%H-%M).tar.gz"
rotate_path="/opt/backup/*"

rotate () {
   rm -f $(ls -1t $rotate_path | tail -n +8)
   rsync -av -–delete -e 'ssh -p 22' $backup_path lenny@10.55.55.16:$backup_path
}

backup () {
  tar -czpvf $(echo $backup_path$backup_name) -C $dir_to_backup . 
}

backup
rotate
```

**Création du timer et service system.d:**

Le fichier `/etc/systemd/system/backup.timer` :
```
$ vi /etc/systemd/system/backup.timer
[21:45]
[Unit]
Description=Run backup.service periodically

[Timer]
# Everyday at 04:00
OnCalendar=--* 15:50:00

[Install]
WantedBy=multi-user.target
```

Le fichier `/etc/systemd/system/backup.service`

``` BASH
$ vi /etc/systemd/system/backup.service
[Unit]
Description=Service Backup

[Service]
ExecStart=/bin/bash backup.sh
user=root
WorkingDirectory=/opt/

[Install]
WantedBy=multi-user.target
```

Activation du Timer:
```bash
$ systemctl disable backup.service
$ systemctl enable backup.timer
$ systemctl start backup.timer
$ systemctl list-timers
NEXT                          LEFT     LAST                          PASSED       UNIT                         ACTIVATES
sam. 2020-12-19 09:33:13 CET  17h left ven. 2020-12-18 09:33:13 CET  6h ago       systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.se
sam. 2020-12-19 15:50:00 CET  23h left ven. 2020-12-18 15:50:08 CET  3min 54s ago backup.timer                 backup.service
```
**Vérification serveur Web:**
``` BASH
$ ls /opt/backup
Wordpress.2020-12-18_15-19.tar.gz  
Wordpress.2020-12-18_15-28.tar.gz  Wordpress.2020-12-18_15-59.tar.gz
Wordpress.2020-12-18_15-23.tar.gz  Wordpress.2020-12-1815-50.tar.gz
Wordpress.2020-12-18_15-28.tar.gz  Wordpress.2020-12-1815-56.tar.gz
```

**Vérification serveur Backup:**
``` BASH
$ ls /opt/backup
Wordpress.2020-12-18_15-19.tar.gz  
Wordpress.2020-12-18_15-28.tar.gz  Wordpress.2020-12-18_15-59.tar.gz
Wordpress.2020-12-18_15-23.tar.gz  Wordpress.2020-12-1815-50.tar.gz
Wordpress.2020-12-18_15-28.tar.gz  Wordpress.2020-12-1815-56.tar.gz
```

# III. Docker

**Installation Docker:**
```bash
$ sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
$ sudo yum install -y yum-utils
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo     
$ sudo yum install docker-ce docker-ce-cli containerd.io
$ sudo systemctl start docker
$ docker run hello-world
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
```                

**Création du conteneur:**
```bash
$ docker pull wordpress
$ docker run --name cesi-wp -p 80:80 -d wordpress
  -e WORDPRESS_DB_HOST=10.55.55.12 \
  -e WORDPRESS_DB_USER=lenny \
  -e WORDPRESS_DB_PASSWORD=fORMATION2020 \
  -e WORDPRESS_DB_NAME=WORDPRESS
```
Le docker est lancé, il suffit de faire la modification dans la configuration du serveur Nginx (`/etc/nginx/nginx.conf`)pour que notre conteneur Wordpress soit en HTTPS
```bash
$ vi /etc/nginx/nginx.conf
    server {
        listen       443 ssl;

        server_name web.cesi;

        ssl_certificate     /etc/pki/tls/certs/cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/cesi.key;

        location / {
            proxy_pass   http://10.55.55.12.49/;
                   }

           }
```
**Redémarrage du service Nginx:**
```bash
$systemctl restart nginx
```
